# Maintainer: Mark Wagie <mark at manjaro dot org>

pkgname=mint-translations
pkgver=2025.01.06
pkgrel=1
pkgdesc="Translation files for some Linux Mint programs"
arch=('any')
url="https://github.com/linuxmint/mint-translations"
license=('GPL-3.0-or-later')
source=("http://packages.linuxmint.com/pool/main/m/${pkgname}/${pkgname}_${pkgver}.tar.xz"
        'makefile.diff')
noextract=("${pkgname}_${pkgver}.tar.xz")
sha256sums=('87ffff50ea4a5b342324d9538874cf14fde41e30463856a708f0f3fd2eb47bdc'
            '2618a58a41db7a00407750a2080b9037b332f1af44903a0396758a82a78e1e53')

prepare() {
  mkdir -p "$pkgname-$pkgver"
  bsdtar xf "${pkgname}_${pkgver}.tar.xz" --strip-components 1 -C "$pkgname-$pkgver"

  cd "$pkgname-$pkgver"
  make clean

  # We only need mint-common & mintstick translations
  patch --strip=1 Makefile < "$srcdir/makefile.diff"
}

build() {
  cd "$pkgname-$pkgver"
  make
}

package() {
  cd "$pkgname-$pkgver"
  cp -r usr "$pkgdir/"
}
